<?php

namespace Drupal\Tests\field_is_empty\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_is_empty\Traits\FieldIsEmptyFieldCreationTrait;

/**
 * Tests for the Field Is Empty field configuration.
 *
 * @group field_is_empty
 */
class FieldIsEmptyDropdownListTest extends BrowserTestBase {

  use FieldIsEmptyFieldCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Content type.
   *
   * @var string
   */
  protected string $type;
  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['node', 'field_ui', 'field_is_empty', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a content type, with underscores.
    $type_name = strtolower($this->randomMachineName(8)) . '_test';
    $type = $this->drupalCreateContentType([
      'name' => $type_name,
      'type' => $type_name,
    ]);
    $this->type = $type->id();

    $field_storage = FieldStorageConfig::create([
      'type' => 'string',
      'entity_type' => 'node',
      'field_name' => 'field_body',
    ]);
    $field_storage->save();

    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $this->type,
    ])->save();

    // Create test user.
    $admin_user = $this->drupalCreateUser([
      'access content',
      'administer node fields',
    ]);
    $this->drupalLogin($admin_user);
  }

  /**
   * Test configuration on add field path.
   */
  public function testFieldIsEmptyConfigs() {
    $entity_fields = array_keys(\Drupal::service('entity_field.manager')->getFieldDefinitions('node', $this->type));
    $bundle_path = 'admin/structure/types/manage/' . $this->type;

    // Add new field on the Manage fields page.
    $this->drupalGet($bundle_path . '/fields/add-field');

    $version = '10.2';
    if (version_compare(\Drupal::VERSION, $version) < 0) {
      $this->assertSession()->optionExists('edit-new-storage-type', 'field_is_empty');

      $field_name = strtolower($this->randomMachineName(8));
      $edit = [
        'new_storage_type' => 'field_is_empty',
        'field_name' => 'field_' . $field_name,
        'label' => $field_name,
      ];
      $this->drupalGet('/admin/structure/types/manage/' . $this->type . '/fields/add-field');
      $this->submitForm($edit, 'Save and continue');
      $this->submitForm([], 'Save field settings');
    }
    else {
      $edit = [
        'new_storage_type' => 'field_is_empty',
      ];
      $this->submitForm($edit, 'Continue');

      $field_name = strtolower($this->randomMachineName(8));

      $edit = [
        'new_storage_type' => 'field_is_empty',
      ];
      $this->submitForm($edit, 'Continue');

      $field_name = strtolower($this->randomMachineName(8));
      $edit = [
        'field_name' => 'field_' . $field_name,
        'label' => $field_name,
      ];
      $this->submitForm($edit, 'Continue');
      $this->submitForm([], 'Save settings');
    }

    $options = $this->getSelectOptionValues('settings[field_name]');
    $this->assertNotContains($field_name, $options);
    $this->assertEqualsCanonicalizing($entity_fields, $options);
  }

  /**
   * Returns a flat list of options the specified select element contains.
   *
   * @param string $name
   *   The field name.
   *
   * @return string[]
   *   The list of option values.
   *
   * @internal
   */
  protected function getSelectOptionValues(string $name): array {
    $field = $this->assertSession()->selectExists($name);
    $options = $field->findAll('css', 'option');
    array_walk($options, function (NodeElement &$option) {
      $option = $option->getAttribute('value');
    });
    $options = array_filter($options);
    return $options;
  }

}
