<?php

namespace Drupal\Tests\field_is_empty\Traits;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Provides a helper method for creating Field is Empty fields.
 */
trait FieldIsEmptyFieldCreationTrait {

  /**
   * Create a new Field is empty field.
   *
   * @param string $name
   *   The name of the new field (all lowercase)
   * @param array $field_settings
   *   (optional) A list of instance settings that will be added to the instance
   *   defaults.
   */
  protected function createFieldIsEmptyField($name, array $field_settings = []) {
    FieldStorageConfig::create([
      'field_name' => $name,
      'entity_type' => 'entity_test',
      'type' => 'field_is_empty',
    ])->save();

    $field_config = FieldConfig::create([
      'field_name' => $name,
      'label' => $name,
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'settings' => $field_settings,
    ]);
    $field_config->save();

    return $field_config;
  }

}
