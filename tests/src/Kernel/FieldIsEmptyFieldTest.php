<?php

namespace Drupal\Tests\field_is_empty\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field_is_empty\Plugin\Field\FieldType\FieldIsEmptyItem;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\Tests\field_is_empty\Traits\FieldIsEmptyFieldCreationTrait;

/**
 * Tests for the Field Is Empty module.
 *
 * @group field_is_empty
 */
class FieldIsEmptyFieldTest extends FieldKernelTestBase {

  use FieldIsEmptyFieldCreationTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['field_is_empty'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $field_storage = FieldStorageConfig::create([
      'type' => 'string',
      'entity_type' => 'entity_test',
      'field_name' => 'field_body',
    ]);
    $field_storage->save();

    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'entity_test',
    ])->save();

    $field_settings = [
      FieldIsEmptyItem::FIELD_NAME_SETTING => 'field_body',
      FieldIsEmptyItem::VALUE_ON_EMPTY_SETTING => TRUE,
      'on_label' => 'Yes',
      'off_label' => 'No',
    ];
    $this->createFieldIsEmptyField('field_is_body_empty', $field_settings);
  }

  /**
   * Tests field output.
   */
  public function testFieldIsEmptyOutput() {
    // Create a test entity.
    $entity = EntityTest::create();
    $entity->name->value = $this->randomMachineName();
    $entity->save();

    $entity = EntityTest::load($entity->id());
    $this->assertEquals($entity->field_is_body_empty->value, 1);

    // Add data to field_body field.
    $entity->field_body->value = $this->randomMachineName(32);
    $entity->save();

    $entity = EntityTest::load($entity->id());
    $this->assertEquals($entity->field_is_body_empty->value, 0);
  }

}
