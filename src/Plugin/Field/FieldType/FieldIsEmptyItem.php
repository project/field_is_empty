<?php

namespace Drupal\field_is_empty\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\BooleanItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the type of field that checks if another field is empty.
 *
 * @FieldType(
 *   id = \Drupal\field_is_empty\Plugin\Field\FieldType\FieldIsEmptyItem::PLUGIN_ID,
 *   label = @Translation("Field is empty"),
 *   description = @Translation("Computed boolean field that depends on whether another field is empty or not."),
 *   default_widget = "boolean_checkbox",
 *   default_formatter = "boolean",
 *   list_class = "\Drupal\field_is_empty\Plugin\Field\FieldType\FieldIsEmptyItemList",
 *   cardinality = 1,
 * )
 *
 * @see \Drupal\field_is_empty\Plugin\Field\FieldType\FieldIsEmptyItemList
 */
class FieldIsEmptyItem extends BooleanItem {

  const PLUGIN_ID = 'field_is_empty';

  const FIELD_NAME_SETTING = 'field_name';

  const VALUE_ON_EMPTY_SETTING = 'empty_value';

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = parent::defaultFieldSettings();

    $settings[static::FIELD_NAME_SETTING] = NULL;
    $settings[static::VALUE_ON_EMPTY_SETTING] = FALSE;

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);

    $definition = $this->getFieldDefinition();
    $entity_type = $definition->getTargetEntityTypeId();
    $bundle = $definition->getTargetBundle();
    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $bundle);
    $fields = [];
    foreach ($field_definitions as $field_name => $field_definition) {
      if ($definition->getName() != $field_name) {
        $fields[$field_name] = $field_definition->getLabel();
      }
    }
    uasort($fields, function ($a, $b) {
      return strnatcasecmp((string) $a, (string) $b);
    });

    $form[static::FIELD_NAME_SETTING] = [
      '#required' => TRUE,
      '#type' => 'select',
      '#options' => $fields,
      '#title' => $this->t('Source field'),
      '#description' => $this->t('The field name to check for emptiness.'),
      '#default_value' => $this->getSetting(static::FIELD_NAME_SETTING),
    ];

    $form[static::VALUE_ON_EMPTY_SETTING] = [
      '#type' => 'radios',
      '#title' => $this->t('Value of this field'),
      '#options' => [
        1 => $this->t('TRUE when source field is empty'),
        0 => $this->t('FALSE when source field is empty'),
      ],
      '#default_value' => $this->getSetting(static::VALUE_ON_EMPTY_SETTING),
    ];

    return $form;
  }

}
