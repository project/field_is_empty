<?php

namespace Drupal\field_is_empty\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Provides the field item list for the Field is Empty field.
 *
 * @see \Drupal\field_is_empty\Plugin\Field\FieldType\FieldIsEmptyItem
 */
class FieldIsEmptyItemList extends FieldItemList {

  use ComputedItemListTrait;

  const FIELD_NAME_SETTING = FieldIsEmptyItem::FIELD_NAME_SETTING;

  const VALUE_ON_EMPTY_SETTING = FieldIsEmptyItem::VALUE_ON_EMPTY_SETTING;

  /**
   * Computes the item value.
   *
   * @return bool|null
   *   TRUE/FALSE depending on the field settings. Returns NULL in case the
   *   source field isn't specified or doesn't exist.
   */
  protected function computeItemValue(): ?bool {
    $field_name = $this->getSetting(static::FIELD_NAME_SETTING);
    if (!$field_name) {
      return NULL;
    }

    $entity = $this->getEntity();
    if (!$entity->hasField($field_name)) {
      return NULL;
    }

    $is_source_empty = $entity->get($field_name)->isEmpty();
    $value_on_empty = $this->getSetting(static::VALUE_ON_EMPTY_SETTING);
    return $is_source_empty ? $value_on_empty : !$value_on_empty;
  }

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $value = $this->computeItemValue();
    if ($value !== NULL) {
      $this->list[] = $this->createItem(0, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    // When field is added through UI as an ordinary and not a computed field,
    // we must trigger computing here, so that it doesn't just save previously
    // calculated value.
    $this->list = [];
    $this->computeValue();

    parent::preSave();
  }

}
